//////////////////////
// Library Includes //
//////////////////////
#include <stdlib.h>
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

//Définition des pins
#define CLK 11
#define LAT 10
#define OE  9

#define A   A0
#define B   A1
#define C   A2
#define D   A3

//Définition des boutons
#define BRP1 12//Button right player 1
#define BLP1 13//Button left player 1
#define BRP2 0//Button right player 2
#define BLP2 1//Button left player 2

//Définition des couleurs
#define RED matrix.Color333(7,0,0)
#define MAG matrix.Color333(7,0,7)
#define BLU matrix.Color333(0,0,7)
#define CYA matrix.Color333(0,7,7)
#define GRE matrix.Color333(0,7,0)
#define YEL matrix.Color333(7,7,0)
#define WHI matrix.Color333(7,7,7)

//Variables

int game = 0;//état de la partie

//Les timers
int timer = 0;//timer général
int maxTimer = 123;//durée de la partie
int timerTimer = 0;//timer secondaire
int loopTimer = 0;
int timerStep = 3;//durée d'une étape de la boucle de jeu

//Etat des boutons
int buttonStateP1R;
int buttonStateP1L;
int buttonStateP2R;
int buttonStateP2L;

int spd = 13000;//vitesse du jeu

//Position du timer contour
int timerX = 31;
int timerY = 31;

//Score des joueurs
int sc1 = 0;
int sc2 = 0;

//Player 1
int cursor1X = 1;//Position du joueur 1
int cursor1Y = 1;
int directionP1 = 1;//Direction du déplacement
//Player 2
int cursor2X = 30;//Position du joueur 2
int cursor2Y = 30;
int directionP2 = 3;//Direction du déplacement

int collision = 0;//variable pour détecter la collision

//Fonction de reset de la partie.
//Elle ré-initialise les différentes variables de la partie.
void reset() {
  game = 0;
  timer = 0;
  maxTimer = 123;
  timerTimer = 0;
  loopTimer = 0;
  timerX = 31;
  timerY = 31;
  sc1 = 0;
  sc2 = 0;
  bonusTimer = 0;
  cursor1X = 1;
  cursor1Y = 1;
  directionP1 = 1;
  cursor2X = 30;
  cursor2Y = 30;
  directionP2 = 3;
}


RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false); //Creation de la matrice 32x32

//Fonction pour dessiner le screen de départ : "PRESS TO PLAY"
void startscreen() {
  matrix.drawChar(1, 3, 'P', YEL, 1, 1);
  matrix.drawChar(7, 3, 'R', YEL, 1, 1);
  matrix.drawChar(13, 3, 'E', YEL, 1, 1);
  matrix.drawChar(19, 3, 'S', YEL, 1, 1);
  matrix.drawChar(25, 3, 'S', YEL, 1, 1);
  matrix.drawChar(10, 12, 'T', YEL, 1, 1);
  matrix.drawChar(16, 12, 'O', YEL, 1, 1);
  matrix.drawChar(4, 21, 'P', YEL, 1, 1);
  matrix.drawChar(10, 21, 'L', YEL, 1, 1);
  matrix.drawChar(16, 21, 'A', YEL, 1, 1);
  matrix.drawChar(22, 21, 'Y', YEL, 1, 1);
}

void setup() {
    //Initialisation de la matrice
    matrix.begin();  
    //Initialisation des pins des boutons
    pinMode(BRP1,INPUT_PULLUP);
    pinMode(BLP1,INPUT_PULLUP);
    pinMode(BRP2,INPUT_PULLUP);
    pinMode(BLP2,INPUT_PULLUP);
    // Start serial
    Serial.begin(9600);
}

void loop() {
    /*Boucle de jeu.
    La variable game symbolise l'état de la partie.
            - 0 = écran d'accueil
            - 1 = écran de lancement de la partie
            - 2 = écran de jeu principal
            - 3 = écran d'affichage des scores
            - 4 = écran de fin
    */
  if (game == 0) {
      //Affichage de l'ecran
    startscreen();
    //Recupération des valeurs des boutons
    buttonStateP1R = digitalRead(BRP1);
    buttonStateP1L = digitalRead(BLP1);
    buttonStateP2R = digitalRead(BRP2);
    buttonStateP2L = digitalRead(BLP2);
    //Si tous les boutons sont pressés, lancement de la partie
    if ((buttonStateP1R == LOW)&&(buttonStateP1L == LOW)&&(buttonStateP2R == LOW)&&(buttonStateP2L == LOW)) {
        game = 1;
    }
  }
 else if (game == 1) {
     //Ecran de lancement de la partie.
     //Affichage d'un compteur
    matrix.fillScreen(0);
    matrix.setTextColor(YEL);
    matrix.setCursor(13,12);
    matrix.print("3");
    delay(1000);
    matrix.fillScreen(0);
    matrix.setCursor(13,12);
    matrix.print("2");
    delay(1000);
    matrix.fillScreen(0);
    matrix.setCursor(13,12);
    matrix.print("1");
    delay(1000);
    matrix.fillScreen(0);
    //Affichage du jeu
    drawPlayer(1);//les joueurs
    drawPlayer(2);
    matrix.drawRect(0,0,32,32,GRE);//le contour
    matrix.drawPixel(31,31,YEL);
    game = 2;
  }
  else if (game == 2) {
      //Boucle principale du jeu
    if(loopTimer < spd) {
        //incrementation du timer de la boucle. Une frame du jeu correspond à Speed frames de l'arduino.
      loopTimer++;
    } else {
      if(timer == maxTimer) {
          //On a atteint la fin de la partie
        game = 3;
      }
      else {
          //Affichage du contour timer
        if(timerTimer == timerStep) {
          timer ++;
          drawTimer();
          timerTimer = 0;
        } else {
          timerTimer ++;
        }
        //Le jeu
        //Lecture des boutons
        buttonStateP1R = digitalRead(BRP1);
        buttonStateP1L = digitalRead(BLP1);
        buttonStateP2R = digitalRead(BRP2);
        buttonStateP2L = digitalRead(BLP2);
        //Joueur 1
        //Changement de la direction selon le bouton pressé
        if(buttonStateP1R == LOW) {
          directionP1++;
        } else if (buttonStateP1L == LOW) {
          directionP1--;
        }
        //Joueur 2
        //Changement de la direction selon le bouton pressé
        if(buttonStateP2R == LOW) {
          directionP2++;
        } else if (buttonStateP2L == LOW) {
          directionP2--;
        } else {
        }
        //Déplacement des joueurs
        movePlayer(1);
        movePlayer(2);
        //Reset du timer de la boucle
        loopTimer = 0;
      }
    }
  }
  else if (game == 3) {
      //Calcul et affichage du score
    int winner = checkWin();
    if(winner == 1){
        //Joueur rouge gagne
      matrix.fillScreen(0);
      matrix.setCursor(4,0);
      matrix.setTextColor(YEL);
      matrix.print("RED");
      matrix.setCursor(4,8);
      matrix.print("WINS");
      matrix.setTextColor(RED);
      matrix.setCursor(4,17);
      matrix.print(sc1);
      matrix.setTextColor(BLU);
      matrix.setCursor(4,25);
      matrix.print(sc2);      
    } else if (winner == 2) {
        //Joueur bleu gagne
      matrix.fillScreen(0);
      matrix.setCursor(4,0);
      matrix.setTextColor(YEL);
      matrix.print("BLUE");
      matrix.setCursor(4,8);
      matrix.print("WINS");
      matrix.setTextColor(RED);
      matrix.setCursor(4,17);
      matrix.print(sc1);
      matrix.setTextColor(BLU);
      matrix.setCursor(4,25);
      matrix.print(sc2);  
    } else {
        //Egalité
      matrix.fillScreen(0);
      matrix.setCursor(4,0);
      matrix.setTextColor(YEL);
      matrix.print("BOTH");
      matrix.setCursor(4,8);
      matrix.print("WIN");
      matrix.setTextColor(RED);
      matrix.setCursor(4,17);
      matrix.print(sc1);
      matrix.setTextColor(BLU);
      matrix.setCursor(4,25);
      matrix.print(sc2);  
    }

    game = 4;
  }
  else if (game = 4) {
      //En appuyant sur les 4 boutons de nouveaux, on relance une partie
        buttonStateP1R = digitalRead(BRP1);
        buttonStateP1L = digitalRead(BLP1);
        buttonStateP2R = digitalRead(BRP2);
        buttonStateP2L = digitalRead(BLP2);
        if ((buttonStateP1R == LOW)&&(buttonStateP1L == LOW)&&(buttonStateP2R == LOW)&&(buttonStateP2L == LOW)){
        reset();
      }
  }
}

//Fonction de dessin du joueur. Le joueur 1 est magenta et le 2 est cyan
void drawPlayer(int player){
  if(player == 1) {
    matrix.drawPixel(cursor1X,cursor1Y,MAG);
  } else {
    matrix.drawPixel(cursor2X,cursor2Y,CYA);
  }
}

//Fonction de déplacement d'un joueur.
void movePlayer(int player) {
    //Joueur 1
  if(player == 1) {
      //Si il y a eu collision, on repart dans l'autre sens
    if(collision == 1){
      directionP1 += 2;
      collision = 0;
    }
    //Si le joueur est sur le bord du plateau, il repart dans l'autre direction.
    if(borders(directionP1,player)) {
      directionP1 += 2;
    }
    int oldX = cursor1X;
    int oldY = cursor1Y;
    //On regarde la direction. On change la position en fonction de la direction.
    switch(directionP1%4) {
      case 0:
        cursor1Y--;
        break;
      case 1:
        cursor1X++;
        break;
      case 2:
        cursor1Y++;
        break;
       case 3:
        cursor1X--;
        break;
    }
    //Dans le cas ou la case correspond aussi à la case du deuxième joueur, il va y  avoir collision.
    if((cursor1X == cursor2X)&&(cursor1Y == cursor2Y)){
      collision = 1;
      directionP1 += 2;
      cursor1X = oldX;
      cursor1Y = oldY;
      //Si on est sur le bord, on rebondi encore
      if(borders(directionP1,player)) {
      directionP1 += 2;
      }
      //Nouveau déplacement
      switch((directionP1)%4) {
        case 0:
          cursor1Y--;
          break;
        case 1:
          cursor1X++;
          break;
        case 2:
          cursor1Y++;
          break;
         case 3:
          cursor1X--;
          break;
      }
      //On dessine le joueur
      drawPlayer(1);
      matrix.drawPixel(oldX,oldY,RED);
    } else {
      drawPlayer(1);
      matrix.drawPixel(oldX,oldY,RED);
    }
  }
  //Le joueur 
  if(player == 2) {
    if(collision == 1){
      directionP2 += 2;
      collision = 0;
    }
    if(borders(directionP2,player)) {
      directionP2 += 2;
    }
    int oldX = cursor2X;
    int oldY = cursor2Y;
    switch(directionP2%4) {
      case 0:
        cursor2Y--;
        break;
      case 1:
        cursor2X++;
        break;
      case 2:
        cursor2Y++;
        break;
       case 3:
        cursor2X--;
        break;
    }
    if((cursor1X == cursor2X)&&(cursor1Y == cursor2Y)){
      collision = 1;
      directionP2 += 2;
      cursor2X = oldX;
      cursor2Y = oldY;
      if(borders(directionP2,player)) {
      directionP2 += 2;
      }
      switch((directionP2)%4) {
        case 0:
          cursor2Y--;
          break;
        case 1:
          cursor2X++;
          break;
        case 2:
          cursor2Y++;
          break;
         case 3:
          cursor2X--;
          break;
      }
      drawPlayer(2);
      matrix.drawPixel(oldX,oldY,BLU);
    } else {
      drawPlayer(2);
      matrix.drawPixel(oldX,oldY,BLU);
    }
  }
}

//Fonction de vérification des bordures. On regarde la futur position du joueur par rapport à la bordure
//Renvoit True si il va dans un bord, False.
bool borders(int dir,int player) {
    //Joueur 1
  if(player == 1) {
    switch(dir%4) {
      case 0:
        if(cursor1Y == 1)
            return true;
        else
          return false;
      case 1:
        if(cursor1X == 30)
            return true;
        else
          return false;
      case 2:
        if(cursor1Y == 30)
            return true;
        else
          return false;   
       case 3:
        if(cursor1X == 1)
            return true;
        else
          return false;             
    }
  }
  //Joueur 2
  if(player == 2) {
    switch(dir%4) {
      case 0:
        if(cursor2Y == 1)
            return true;
        else
          return false;
      case 1:
        if(cursor2X == 30)
            return true;
        else
          return false;
      case 2:
        if(cursor2Y == 30)
            return true;
        else
          return false;   
       case 3:
        if(cursor2X == 1)
            return true;
        else
          return false;             
    }
  }
}

//Fonction de dessin du timer.
void drawTimer() {
  int dir = 0;
  //Changement de direction
  if(timer < 32) {
    dir = 3;
  }
  //Changement de direction
  else if (timer < 63) {
    dir = 0;
  }
  //Changement de direction
  else if (timer < 94) {
    dir = 1;
  } else {
    dir = 2;
  }
  //On déplace le timer
  switch(dir) {
    case 0:
      timerY--;
      break;
    case 1:
      timerX++;
      break;
    case 2:
      timerY++;
      break;
    case 3:
      timerX--;
      break;
  }
  matrix.drawPixel(timerX,timerY,YEL);
}

//Fonction de vérification du score.
//On parcourt l'ensemble du panneau de LED, et on regarde les couleurs.
int checkWin() {
  for (int i = 1; i < 31; i++) {
     for (int j = 1; j < 31; j++) {
         //Si la LED est bleu ou cyan, c'est pour le joueur 2
        if((getPixelColor(i,j) == 30) || (getPixelColor(i,j) == 1950)){
          sc2 ++;
        }
        // Si elle est rouge ou magenta, c'est pour le joueur 1
        if((getPixelColor(i,j) == 61440) || (getPixelColor(i,j) == 61470)){
          sc1 ++;
        }
     }
  }
  //On compare les scores.
  if(sc1>sc2){
    return 1;
  }
  if(sc2>sc1){
    return 2;
  }
  else {
    return 0;
  }
}

//Fonction de récupération de la couleur d'un pixel.
uint16_t getPixelColor(uint8_t x, uint8_t y)
{
  uint8_t * ptr; // A movable pointer to read data from
  uint16_t r = 0, g = 0, b = 0; // We'll store rgb values in these
  uint8_t nPlanes = 4; // 4 planes. This is static in the library
  uint8_t nRows = matrix.height() / 2; // 16 for 32 row, 8 for 16

  // Get a pointer to the matrix backBuffer, where the pixel/
  // color data is stored:
  uint8_t * buf = matrix.backBuffer();

  // Data for upper half is stored in lower bits of each byte
  if (y < nRows)
  {
    ptr = &buf[y * matrix.width() * (nPlanes - 1) + x];
    // get data from plane 0:
    if (ptr[64] & 0x01)
      r = 1;
    if (ptr[64] & 0x02)
      g = 1;
    if (ptr[32] & 0x01)
      b = 1;
    // get data from other planes:
    for (uint8_t i = 2; i < (1 << nPlanes);  i <<= 1)
    {
      if (*ptr & 0x04)
        r |= i;
      if (*ptr & 0x08)
        g |= i;
      if (*ptr & 0x10)
        b |= i;
      ptr += matrix.width();
    }
  }
  else
  {
    ptr = &buf[(y - nRows) * matrix.width() * (nPlanes - 1) + x];
    // get data from plane 0
    if (ptr[32] & 0x02)
      r = 1;
    if (*ptr & 0x01)
      g = 1;
    if (*ptr & 0x02)
      b = 1;
    for (uint8_t i = 2; i < (1 << nPlanes); i <<= 1)
    {
      if (*ptr & 0x20)
        r |= i;
      if (*ptr & 0x40)
        g |= i;
      if (*ptr & 0x80)
        b |= i;
      ptr += matrix.width();
    }
  }
  return (r << 12) | (g << 7) | (b << 1);
}
