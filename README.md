# Snakoon

## Gameplay
* Au démarrage, les 2 joueurs appuient sur les 4 boutons (2 chacun).
* Après un petit compte-à-rebours, le jeu commence.
* Chaque joueur doit, à l'aide de ses boutons, colorier avec sa couleur le plus de pixels possibles en un temps limité. Le timer est représenté par le contour changeant de couleur. Le joueur 1 est magenta et colorie en rouge ; le joueur 2 est cyan et colorie en bleu.
* A la fin du temps imparti, le score s'affiche et les joueurs peuvent appuyer sur les boutons pour recommencer une partie.


## Mise en oeuvre
* Brancher le panneau de LED
* Brancher les quatre boutons sur les ports 0, 1, 12 et 13 de la carte Arduino
* Alimenter la carte Arduino et le panneau de LED

## Choix
Nous avons seulement utilisé du matériel déjà disponible, sans faire d'achat.
Nous avons choisi d'utiliser une carte Arduino pour sa simplicité d'emploi.